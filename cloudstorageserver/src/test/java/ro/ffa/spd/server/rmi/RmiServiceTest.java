package ro.ffa.spd.server.rmi;

import org.junit.Assert;
import org.junit.Test;
import ro.ffa.spd.common.RemoteFileOperator;
import ro.ffa.spd.server.service.FileServiceProvider;

import java.rmi.registry.LocateRegistry;

import static org.mockito.Mockito.*;

public class RmiServiceTest {

    @Test
    public void testDownloadRmi() throws Exception{
        final FileServiceProvider fileService = mock(FileServiceProvider.class);
        when(fileService.downloadFile(anyString())).thenReturn("parrot".getBytes());

        final RmiService rmiService = new RmiService(1099, fileService, "fileService");
        rmiService.start();
        final RemoteFileOperator stub = (RemoteFileOperator) LocateRegistry.getRegistry().lookup("fileService");

        Assert.assertEquals("parrot", new String(stub.downloadFile("whatever")));
        rmiService.stop(false);
    }

}
