package ro.ffa.spd.server.service;

import org.junit.Assert;
import org.junit.Test;
import ro.ffa.spd.common.RemoteFileOperator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileServiceProviderTest {
    private static final String FIRST_LEVEL_FOLDER_PATH = "/papagal";
    private static final String SECOND_LEVEL_FOLDER_PATH = "/papagal/shaur";
    private static final String SECOND_LEVEL_SECOND_FOLDER_PATH = "/papagal/tambal";
    private static final String SECOND_LEVEL_FILE_PATH = "/papagal/file.txt";
    private static final String ROOT_PATH = ".";

    private RemoteFileOperator fileOperator = new FileServiceProvider(ROOT_PATH);

    @Test
    public void testCreateListAndRemove() throws Exception{
        try {

            fileOperator.createDirectory(FIRST_LEVEL_FOLDER_PATH);
            fileOperator.createDirectory(SECOND_LEVEL_FOLDER_PATH);
            fileOperator.createDirectory(SECOND_LEVEL_SECOND_FOLDER_PATH);

            Assert.assertEquals(2, fileOperator.listFiles(FIRST_LEVEL_FOLDER_PATH).length);

            fileOperator.removeDirectory(SECOND_LEVEL_SECOND_FOLDER_PATH);
            fileOperator.removeDirectory(SECOND_LEVEL_FOLDER_PATH);

            Assert.assertEquals(0, fileOperator.listFiles(FIRST_LEVEL_FOLDER_PATH).length);

        } finally { // should be separate I guess
            Files.deleteIfExists(Paths.get(ROOT_PATH + SECOND_LEVEL_FOLDER_PATH));
            Files.deleteIfExists(Paths.get(ROOT_PATH + SECOND_LEVEL_SECOND_FOLDER_PATH));
            Files.deleteIfExists(Paths.get(ROOT_PATH + FIRST_LEVEL_FOLDER_PATH));
        }
    }

    @Test
    public void testUploadDownloadAndRemove() throws IOException {
        try {

            final String dummyContent = "dummy content";
            fileOperator.createDirectory(FIRST_LEVEL_FOLDER_PATH);
            fileOperator.uploadFile(SECOND_LEVEL_FILE_PATH, dummyContent.getBytes());

            Assert.assertEquals(dummyContent, new String(fileOperator.downloadFile(SECOND_LEVEL_FILE_PATH)));

            fileOperator.removeFile(SECOND_LEVEL_FILE_PATH);
            Assert.assertEquals(0, fileOperator.listFiles(FIRST_LEVEL_FOLDER_PATH).length);

        } finally {
            Files.deleteIfExists(Paths.get(ROOT_PATH + SECOND_LEVEL_FILE_PATH));
            Files.deleteIfExists(Paths.get(ROOT_PATH + FIRST_LEVEL_FOLDER_PATH));
        }
    }

    @Test
    public void testIsDirectory() throws Exception {
        try {
            fileOperator.createDirectory(FIRST_LEVEL_FOLDER_PATH);
            Assert.assertTrue(fileOperator.isDirectory(FIRST_LEVEL_FOLDER_PATH));
        } finally {
            Files.deleteIfExists(Paths.get(ROOT_PATH + FIRST_LEVEL_FOLDER_PATH));
        }
    }

}
