package ro.ffa.spd.server.util;

import com.beust.jcommander.Parameter;

public class Arguments {
    @Parameter(names = {"--disableRmi", "-dr"}, description = "Disable RMI service flag")
    private boolean rmiDisabled = false;

    @Parameter(names = {"--disableWeb", "-dw"}, description = "Disable Web server flag")
    private boolean webDisabled = false;

    @Parameter(names = {"--rmiPort", "-rp"}, description = "Port on which RMI should run")
    private int rmiPort = 1099;

    @Parameter(names = {"--webPort", "-wp"}, description = "Port on which the web server should run")
    private int webPort = 8080;

    @Parameter(names = {"--rmiServiceName", "-rsn"}, description = "RMI service name that will be used to register the service")
    private String rmiServiceName = "fileService";

    @Parameter(names = {"--rootFolder", "-rf"}, description = "Root Folder where the service will store files and folders (default current folder)")
    private String rootFolderPath = ".";

    @Parameter(names = "--help", description = "Print help message")
    private boolean help = false;

    public boolean isRmiDisabled() {
        return rmiDisabled;
    }

    public void setRmiDisabled(boolean rmiDisabled) {
        this.rmiDisabled = rmiDisabled;
    }

    public boolean isWebDisabled() {
        return webDisabled;
    }

    public void setWebDisabled(boolean webDisabled) {
        this.webDisabled = webDisabled;
    }

    public int getRmiPort() {
        return rmiPort;
    }

    public void setRmiPort(int rmiPort) {
        this.rmiPort = rmiPort;
    }

    public int getWebPort() {
        return webPort;
    }

    public void setWebPort(int webPort) {
        this.webPort = webPort;
    }

    public String getRmiServiceName() {
        return rmiServiceName;
    }

    public void setRmiServiceName(String rmiServiceName) {
        this.rmiServiceName = rmiServiceName;
    }

    public String getRootFolderPath() {
        return rootFolderPath;
    }

    public void setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
    }

    public boolean isHelp() {
        return help;
    }

    public void setHelp(boolean help) {
        this.help = help;
    }

    @Override
    public String toString() {
        return "Arguments{" +
                "rmiDisabled=" + rmiDisabled +
                ", webDisabled=" + webDisabled +
                ", rmiPort=" + rmiPort +
                ", webPort=" + webPort +
                ", rmiServiceName='" + rmiServiceName + '\'' +
                ", rootFolderPath='" + rootFolderPath + '\'' +
                ", help=" + help +
                '}';
    }
}
