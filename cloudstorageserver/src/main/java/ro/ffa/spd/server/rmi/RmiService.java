package ro.ffa.spd.server.rmi;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RmiService {
    private static final Logger LOGGER = Logger.getLogger(RmiService.class.getName());

    private final int rmiServicePort;
    private Registry registry;
    private final Remote service;
    private final String serviceName;

    public RmiService(int rmiServicePort, Remote service, String serviceName) {
        this.rmiServicePort = rmiServicePort;
        this.serviceName = serviceName;
        this.service = service;
    }

    public void start() {
        try {
            registry = LocateRegistry.createRegistry(rmiServicePort);
            final Remote stub = UnicastRemoteObject.exportObject(service, rmiServicePort);
            registry.rebind(serviceName, stub);
        } catch (RemoteException e) {
            LOGGER.log(Level.SEVERE, "Exception while starting RMI service", e);
        }
    }

    public void stop(boolean force) {
        try {
            registry.unbind(serviceName);
            UnicastRemoteObject.unexportObject(service, force);
            UnicastRemoteObject.unexportObject(registry, force);
        } catch (RemoteException | NotBoundException e) {
            LOGGER.log(Level.SEVERE, "Exception while stopping RMI service", e);
        }
    }

}
