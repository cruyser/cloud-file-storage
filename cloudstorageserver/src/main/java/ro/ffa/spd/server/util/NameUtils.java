package ro.ffa.spd.server.util;

import java.util.Arrays;

public class NameUtils {

    public static String createNewPath(String currentFolderPath, String newElementName) {
        final String delimiter = "/".equals(currentFolderPath) ? "" : "/";
        return currentFolderPath + delimiter + newElementName;
    }

    public static String buildParentPath(String currentPath) {
        if ("/".equals(currentPath)) return null;
        else {
            if (currentPath.substring(1).contains("/")) return currentPath.substring(0, currentPath.lastIndexOf('/'));
            else return "/";
        }
    }

    public static String[] listFileNames(String[] filePaths) {
        return Arrays.stream(filePaths)
                .map(filePath -> filePath.substring(filePath.lastIndexOf('/') + 1))
                .toArray(String[]::new);
    }

}
