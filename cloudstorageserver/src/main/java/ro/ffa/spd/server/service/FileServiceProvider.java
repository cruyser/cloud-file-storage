package ro.ffa.spd.server.service;

import ro.ffa.spd.common.RemoteFileOperator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FileServiceProvider implements RemoteFileOperator {
    private final Path rootPath;

    public FileServiceProvider(String rootPath) {
        this.rootPath = Paths.get(rootPath).toAbsolutePath();
    }

    @Override
    public boolean isDirectory(String path) {
        return getFullPath(path).toFile().isDirectory();
    }

    @Override
    public byte[] downloadFile(String path) throws IOException {
        return Files.readAllBytes(getFullPath(path));
    }

    @Override
    public String[] listFiles(String path) throws IOException {
        try (Stream<Path> files = Files.list(getFullPath(path))) {
            return files.map(rootPath::relativize)
                    .map(FileServiceProvider::transformToUnixPathString)
                    .map(FileServiceProvider::transformToAbsolute)
                    .toArray(String[]::new);
        }
    }

    @Override
    public void createDirectory(String path) throws IOException {
        Files.createDirectory(getFullPath(path));
    }

    @Override
    public void removeDirectory(String path) throws IOException {
        Files.delete(getFullPath(path));
    }

    @Override
    public void removeFile(String path) throws IOException {
        Files.delete(getFullPath(path));
    }

    @Override
    public void uploadFile(String path, byte[] data) throws IOException {
        Files.write(getFullPath(path), data, StandardOpenOption.CREATE_NEW);
    }

    public String getMimeType(String path) throws IOException {
        return Files.probeContentType(getFullPath(path));
    }

    private static String transformToRelative(String path) {
        if (path.indexOf('/') != 0) throw new IllegalArgumentException("Path is not absolute");
        return path.substring(1);
    }

    private Path getFullPath(String path) {
        return rootPath.resolve(transformToRelative(path)).toAbsolutePath();
    }

    private static String transformToAbsolute(String path) {
        return "/" + path;
    }

    private static String transformToUnixPathString(Path path) {
        return IntStream
                .range(0, path.getNameCount())
                .mapToObj(path::getName)
                .map(String::valueOf)
                .collect(Collectors.joining("/"));
    }
}
