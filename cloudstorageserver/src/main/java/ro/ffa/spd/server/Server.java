package ro.ffa.spd.server;

import com.beust.jcommander.JCommander;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ro.ffa.spd.server.web.controller.FileController;
import ro.ffa.spd.server.rmi.RmiService;
import ro.ffa.spd.server.service.FileServiceProvider;
import ro.ffa.spd.server.util.Arguments;

import java.io.PrintWriter;

@SpringBootApplication
@ComponentScan("ro.ffa.spd.server.web")
public class Server {
    private static final PrintWriter OUT =
            System.console() != null ? System.console().writer() : new PrintWriter(System.out, true);

    public static void main(String[] args) {

        final Arguments argv = new Arguments();
        final JCommander jCommander = JCommander
                .newBuilder()
                .addObject(argv)
                .build();
        jCommander.parse(args);
        if (argv.isHelp()) {
            jCommander.usage();
            return;
        }

        final FileServiceProvider fileService = new FileServiceProvider(argv.getRootFolderPath());

        if (argv.isRmiDisabled() && argv.isWebDisabled()) {
            OUT.println("Nothing to do...");
            return;
        }

        if (!argv.isWebDisabled()) {
            final ConfigurableApplicationContext springContext =
                    SpringApplication.run(Server.class, "--server.port=" + argv.getWebPort());
            springContext.getBean("fileController", FileController.class).setFileService(fileService);
        }

        if (!argv.isRmiDisabled()) {
            final RmiService rmiService = new RmiService(argv.getRmiPort(), fileService, argv.getRmiServiceName());
            rmiService.start();
            OUT.println("[Started RMI service successfully]");
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                rmiService.stop(false);
                OUT.println("[Successfully stopped RMI service]");
            }));
        }
    }

}
