package ro.ffa.spd.server.web.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ro.ffa.spd.server.service.FileServiceProvider;
import ro.ffa.spd.server.util.NameUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Controller
@RequestMapping("/storage")
public class FileController {
    private static final String CONTEXT_PATH = "/storage"; //NOSONAR
    private FileServiceProvider fileService;

    @GetMapping(value = "/**")
    public String list(
            HttpServletRequest request, Model model) throws IOException {

        final String path = extractFilePath(request.getServletPath());
        final String parentPath = NameUtils.buildParentPath(path);
        model.addAttribute("parentPath", parentPath == null ? null : prependContext(parentPath));
        if (fileService.isDirectory(path)) {
            final String[] filePaths = fileService.listFiles(path);
            model.addAttribute("currentFolder", path);
            model.addAttribute("filePaths", Arrays.stream(filePaths).map(this::prependContext).toArray(String[]::new));
            model.addAttribute("fileNames", NameUtils.listFileNames(filePaths));
            return "list";
        } else {
            request.setAttribute("filePath", path);
            return "forward:/storage/download";
        }
    }

    @GetMapping(value = "/download")
    public void download(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String path = (String) request.getAttribute("filePath");
        response.setContentType(fileService.getMimeType(path));
        final byte[] content = fileService.downloadFile(path);
        response.setContentLength(content.length);
        response.setHeader("Content-disposition",
                "inline;filename=" + path.substring(path.lastIndexOf('/') + 1)); // inline or attachment
        response.getOutputStream().write(content);
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam MultipartFile file, @RequestParam String currentFolderPath) throws IOException {
        fileService.uploadFile(NameUtils.createNewPath(currentFolderPath, file.getOriginalFilename()), file.getBytes());
        return "redirect:" + prependContext(currentFolderPath);
    }

    @PostMapping("/createFolder")
    public String createFolder(@RequestParam String newFolderName, @RequestParam String currentFolderPath) throws IOException{
        fileService.createDirectory(NameUtils.createNewPath(currentFolderPath, newFolderName));
        return "redirect:/storage" + currentFolderPath;
    }

    @DeleteMapping("/**")
    public ResponseEntity<String> delete(HttpServletRequest request) throws IOException{
        final String path = extractFilePath(request.getServletPath());
        if (fileService.isDirectory(path)) fileService.removeDirectory(path);
        else fileService.removeFile(path);
        return ResponseEntity.ok("deleted");
    }

    public FileServiceProvider getFileService() {
        return fileService;
    }

    public void setFileService(FileServiceProvider fileService) {
        this.fileService = fileService;
    }

    private String extractFilePath(String originalPath) {
        return CONTEXT_PATH.equals(originalPath) ? "/" : originalPath.substring(CONTEXT_PATH.length());
    }

    private String prependContext(String path) {
        return CONTEXT_PATH + path;
    }
}
