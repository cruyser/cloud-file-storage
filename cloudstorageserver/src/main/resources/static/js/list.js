function remove() {
    var removePath = document.getElementById("removePath").value;
    var currentFolderPath = document.getElementById("currentFolderPath").value;
    var csrf = document.getElementById('csrfToken');

    fetch(buildPath(currentFolderPath, removePath) + '?' + csrf.name + '=' + csrf.value, {
        method: "DELETE"
    }).then(function () { window.location.replace('/storage' + currentFolderPath); });
}

function buildPath(currentPath, fileName) {
    var delimiter = currentPath === '/' ? '' : '/';
    return '/storage' + currentPath + delimiter + fileName;
}