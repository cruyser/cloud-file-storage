package ro.ffa.spd.common;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteFileOperator extends Remote {

    byte[] downloadFile(String path) throws IOException;

    String[] listFiles(String path) throws IOException;

    void createDirectory(String path) throws IOException;

    void removeDirectory(String path) throws IOException;

    void removeFile(String path) throws IOException;

    void uploadFile(String path, byte[] data) throws IOException;

    boolean isDirectory(String path) throws RemoteException;

}
