package ro.ffa.spd.service;

public enum Command {
    PRINT_WORKING_DIR("pwd", 0, 0),
    LIST("ls", 0, 1),
    CREATE_DIRECTORY("mkdir", 1, 1),
    REMOVE("rm", 1, 1),
    UPLOAD_FILE("up", 1, 2),
    DOWNLOAD_FILE("down",1, 2),
    CHANGE_DIR("cd", 0, 1),
    EXIT("exit", 0, 0);

    final String name;
    final int minArgs;
    final int maxArgs;

    Command(String name, int minArgs, int maxArgs) {
        this.name = name;
        this.minArgs = minArgs;
        this.maxArgs = maxArgs;
    }

    public boolean testArgsRange(int argsNumber) {
        return argsNumber >= minArgs && argsNumber <= maxArgs;
    }

    public static Command parse(String name) {
        for (Command command : Command.values()) {
            if (command.name.equals(name)) return command;
        }
        throw new IllegalArgumentException("No such command: " + name);
    }
}
