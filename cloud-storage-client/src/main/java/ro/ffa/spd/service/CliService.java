package ro.ffa.spd.service;

import ro.ffa.spd.common.RemoteFileOperator;

import java.io.Console;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class CliService {

    private final RemoteFileOperator fileOperator;
    private final Console console;
    private boolean running;
    private String currentDir;

    public CliService(RemoteFileOperator fileOperator, Console console) {
        this.fileOperator = fileOperator;
        this.console = console;
        this.running = true;
        currentDir = "/";
    }

    public void start() {
        while (running) {
            try {
                final String command = console.readLine(" > ");
                parseCommand(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseCommand(String cmd) throws IOException {
        final String[] inputs = cmd.split(" ");
        final Command command = Command.parse(inputs[0]);
        final String[] args = Arrays.copyOfRange(inputs, 1, inputs.length);

        if (!command.testArgsRange(args.length)) {
            console.format("Invalid number of arguments: required [%d-%d] but got %d",
                    command.minArgs, command.maxArgs, args.length);
            console.flush();
            return;
        }

        switch (command) {
            case LIST:
                list(args);
                break;
            case REMOVE:
                remove(args);
                break;
            case CREATE_DIRECTORY:
                fileOperator.createDirectory(parseArgument(args[0]));
                break;
            case UPLOAD_FILE:
                upload(args);
                break;
            case DOWNLOAD_FILE:
                download(args);
                break;
            case CHANGE_DIR:
                if (args.length == 0) currentDir = "/";
                else if (args[0].equals("..")) changeDirectoryUp(currentDir);
                    else currentDir = parseArgument(args[0]);
                break;
            case PRINT_WORKING_DIR:
                console.writer().println(currentDir);
                console.flush();
                break;
            case EXIT:
                running = false;
                break;
            default:
                console.writer().println("Unknown command");
                console.flush();
        }
    }

    private String parseArgument(String arg) {
        if (arg.indexOf('/') == 0) return arg;
        if ("/".equals(currentDir)) return currentDir + arg;
        else return currentDir + "/" + arg;
    }

    private void changeDirectoryUp(String arg) {
        if (arg.lastIndexOf('/') == 0) currentDir = "/";
        else currentDir = currentDir.substring(0, currentDir.lastIndexOf('/'));
    }

    private void list(String... args) throws IOException {
        final String path = args.length == Command.LIST.minArgs ? currentDir : parseArgument(args[0]);
        final String[] elements = fileOperator.listFiles(parseArgument(path));
        for (String element : elements) {
            String displayText = element.substring(currentDir.length());
            displayText = displayText.startsWith("/") ? displayText.substring(1) : displayText;
            console.writer().println('\t' + displayText);
        }
        console.flush();
    }

    private void remove(String... args) throws IOException {
        final String path = parseArgument(args[0]);
        if (fileOperator.isDirectory(path)) fileOperator.removeDirectory(path);
        else fileOperator.removeFile(path);
    }

    private void upload(String... args) throws IOException {
        args[0] = args[0].replaceAll("\"", "");
        final Path path = Paths.get(args[0]);
        final String remotePath =
                args.length == Command.UPLOAD_FILE.maxArgs
                        ? parseArgument(args[1])
                        : parseArgument(path.getFileName().toString());
        fileOperator.uploadFile(remotePath, Files.readAllBytes(path));
    }

    private void download(String... args) throws IOException {
        final Path localPath = Command.DOWNLOAD_FILE.maxArgs == args.length
                ? Paths.get(args[1])
                : Paths.get(".", args[0].substring(Math.max(0, args[0].lastIndexOf('/'))));
        Files.write(localPath, fileOperator.downloadFile(parseArgument(args[0])));
    }
}
