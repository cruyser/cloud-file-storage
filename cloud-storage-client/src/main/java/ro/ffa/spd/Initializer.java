package ro.ffa.spd;

import com.beust.jcommander.JCommander;
import ro.ffa.spd.client.util.Arguments;
import ro.ffa.spd.common.RemoteFileOperator;
import ro.ffa.spd.service.CliService;

import java.io.Console;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Initializer {

    public static void main(String[] args) throws RemoteException, NotBoundException {
        final Arguments arg = new Arguments();
        final JCommander jCommander = JCommander.newBuilder()
                .addObject(arg)
                .build();
        jCommander.parse(args);
        if (arg.isHelp()) {
            jCommander.usage();
            return;
        }

        final Console console = System.console();
        if (console == null) throw new IllegalStateException("No console to write to");

        final RemoteFileOperator fileOperatorStub = (RemoteFileOperator)
                LocateRegistry.getRegistry(arg.getHost(), arg.getPort()).lookup(arg.getServiceName());

        new CliService(fileOperatorStub, console).start();
    }

}
