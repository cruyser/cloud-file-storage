package ro.ffa.spd.client.util;

import com.beust.jcommander.Parameter;

public class Arguments {

    @Parameter(names = {"--host", "-h"}, description = "Remote RMI service host")
    private String host = "localhost";

    @Parameter(names = {"--port", "-p"}, description = "Remote RMI service port")
    private int port = 1099;

    @Parameter(names = {"--service", "-s"}, description = "Service name to look up")
    private String serviceName = "fileService";

    @Parameter(names = "--help", description = "Print help message")
    private boolean help = false;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isHelp() {
        return help;
    }

    public void setHelp(boolean help) {
        this.help = help;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
